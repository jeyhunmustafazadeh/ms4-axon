package az.ingress.ms4axon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms4AxonApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ms4AxonApplication.class, args);
    }

}
