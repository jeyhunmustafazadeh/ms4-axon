package az.ingress.cardsms.projector;

import az.ingress.cardsms.repository.UserRepository;
import az.ingress.common.entity.User;
import az.ingress.common.event.UserCreatedEvent;
import lombok.RequiredArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserProjector {

    private final UserRepository repository;

    @EventHandler
    public void on(UserCreatedEvent event) {
        var user = User.builder()
                .id(event.getUserId())
                .displayName(event.getDisplayName())
                .email(event.getEmail())
                .password(event.getPassword())
                .build();
        repository.save(user);
    }

}

