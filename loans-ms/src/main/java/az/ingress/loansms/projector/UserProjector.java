package az.ingress.loansms.projector;

import az.ingress.common.entity.User;
import az.ingress.common.event.UserCreatedEvent;
import az.ingress.loansms.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserProjector {

    private final UserRepository repository;

    @EventHandler
    public void on(UserCreatedEvent event) {
        System.out.println(event);
        var user = User.builder()
                .id(event.getUserId())
                .displayName(event.getDisplayName())
                .email(event.getEmail())
                .password(event.getPassword())
                .build();
        repository.save(user);
    }

}

