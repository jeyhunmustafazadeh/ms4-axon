package az.ingress.common.command;

import lombok.Data;

@Data
public class CreateUserCommand {
    private String displayName;
    private String email;
    private String password;
}
