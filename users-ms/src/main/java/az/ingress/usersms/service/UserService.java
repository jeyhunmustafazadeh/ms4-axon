package az.ingress.usersms.service;

import az.ingress.common.command.CreateUserCommand;
import az.ingress.common.entity.User;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class UserService {
    private final CommandGateway commandGateway;
    private final QueryGateway queryGateway;


    public UUID save(CreateUserCommand command) {
        return commandGateway.sendAndWait(command);
    }


    public CompletableFuture<User> getUser(String id ) {
        return queryGateway.query(UUID.fromString(id),
                ResponseTypes.instanceOf(User.class));
    }
}
