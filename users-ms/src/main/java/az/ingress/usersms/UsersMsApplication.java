package az.ingress.usersms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;



@EnableJpaRepositories("az.ingress.*")
@ComponentScan(basePackages = { "az.ingress.*" })
@EntityScan({"az.ingress.*","org.axonframework.eventhandling.tokenstore.jpa"})
@SpringBootApplication
public class UsersMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsersMsApplication.class, args);
    }

}
