package az.ingress.usersms.projector;


import az.ingress.common.entity.User;
import az.ingress.common.event.UserCreatedEvent;
import az.ingress.usersms.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class UserProjector {

    private final UserRepository repository;

    @EventHandler
    public void on(UserCreatedEvent event) {
        var user = User.builder()
                .id(event.getUserId())
                .displayName(event.getDisplayName())
                .email(event.getEmail())
                .password(event.getPassword())
                .build();
        repository.save(user);
    }

    @QueryHandler
    @Transactional
    public User handle(UUID query) {
        User user = repository.findById(query).orElse(null);
        return user;
    }


}

