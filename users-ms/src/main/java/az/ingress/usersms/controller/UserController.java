package az.ingress.usersms.controller;

import az.ingress.common.command.CreateUserCommand;
import az.ingress.common.entity.User;
import az.ingress.usersms.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity<UUID> save (@RequestBody CreateUserCommand command){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(userService.save(command));
    }

    @GetMapping("/{id}")
    public CompletableFuture<User> getUser(@PathVariable String id ) {
        return userService.getUser(id);
    }
}
